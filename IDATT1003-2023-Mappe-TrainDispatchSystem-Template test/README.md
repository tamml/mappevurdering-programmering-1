# Portfolio project IDATA1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = "Tam Minh Le"  
STUDENT ID = "111709"

## Project description

[//]: # This is the final project for the class IDATT1OO3 (Programmering 1). The task was to create a interactive train departure system. The project uses a text based user interface with a couple of mandatory user choices and the code incorporates robust error handling. 

## Project structure

[//]: # The project is started in the TrainDispatchApp class with a couple of prebuild train departures. The TrainDeparture class is the class that stores the set and get methods that are used in the methods in the other classes. The TrainDepartureRegister is responsible for defining methods used in the UserInterface class, this class is dedicated to store and retrieve train departure information. The UserInterface class is where the user interacts with the program, the class has a meny where the user can choose specific actions that they want to do with the train departures. The UserInterface is also where the main methods for the code are. The test are stored in the src files under test/java/edu/ntnu/stud each corresponding to the classes they validate. 

(TODO: Describe the structure of your project here. How have you used packages in your structure. Where are all sourcefiles stored. Where are all JUnit-test classes stored. etc.)

## Link to repository

[//]: # https://gitlab.stud.idi.ntnu.no/tamml/mappevurdering-programmering-1

## How to run the project

[//]: # You can either run the project from the TrainDispatchApp or use maven with the command: mvn exec:java in terminal make sure that you are in the right directory, for me the terminal will look like this: PS C:\Users\tamle\Documents\Mappevurdering Høst 2023 1 Semester\IDATT1003-2023-Mappe-TrainDispatchSystem-Template test> mvn exec:java 

## How to run the tests

[//]: # You can either run the test by pressing the run java button in the test classes or run them from the terminal using maven with the command: mvn test  
Once again make sure that you are in the right directory. For me it will look like this: PS C:\Users\tamle\Documents\Mappevurdering Høst 2023 1 Semester\IDATT1003-2023-Mappe-TrainDispatchSystem-Template test> mvn test  
