package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.stud.TrainDeparture;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrainDepartureTest {
    
  private TrainDeparture trainDeparture;
     
  @BeforeEach
    void setUp() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDeparture =
         new TrainDeparture(departureTime, line, trainNumber, destination, delay, track, null);
  }
    
  @Test
      void testGetDepartureTime() {
    LocalTime departureTime = LocalTime.of(8, 0);
    assertEquals(departureTime, trainDeparture.getDepartureTime());
  }
    
  @Test
      void testGetLine() {
    String line = "Line1";
    assertEquals(line, trainDeparture.getLine());
  }
      
  @Test
    void testGetTrainNumber() {
    String trainNumber = "Train1";
    assertEquals(trainNumber, trainDeparture.getTrainNumber());
  }
      
  @Test
      void testGetDestination() {
    String destination = "Destination1";
    assertEquals(destination, trainDeparture.getDestination());
  }
    
  @Test
      void testGetDelay() {
    int delay = 0;
    assertEquals(delay, trainDeparture.getDelay());
  }
    
  @Test
      void testGetTrack() {
    int track = 1;
    assertEquals(track, trainDeparture.getTrack());
  }
    
  @Test
      void testGetUpdatedDepartureTime() {
    LocalTime updatedDepartureTime = LocalTime.of(8, 0);
    assertEquals(updatedDepartureTime, trainDeparture.getUpdatedDepartureTime());
  }
    
  @Test
      void testSetDepartureTime() {
    LocalTime departureTime = LocalTime.of(8, 0);
    trainDeparture.setDepartureTime(departureTime);
    assertEquals(departureTime, trainDeparture.getDepartureTime());
  }

  @Test
        void testSetDelay() {
    int delay = 10;
    trainDeparture.setDelay(delay);
    assertEquals(delay, trainDeparture.getDelay());
  }

  @Test
        void testSetDelayNegativeDelay() {
    int delay = -10;
    assertThrows(IllegalArgumentException.class, () -> trainDeparture.setDelay(delay));
  }
}

    

