package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.TrainDepartureRegister;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TrainDepartureRegisterTest {

  private TrainDepartureRegister trainDepartureRegister;
   
  @BeforeEach
    void setUp() {
    trainDepartureRegister = new TrainDepartureRegister();
  }

  @Test
  void testCreateNewTrainDeparture() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;

    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);

    assertTrue(trainDepartureRegister.doesTrainDepartureExist(trainNumber));
  }

  @Test
    void testCreateNewTrainDepartureWithExistingTrainNumber() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;

    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);
    
    assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.createNewTrainDeparture(
              departureTime, line, trainNumber, destination, delay, track));
  }

  @Test
    void testChangeLineForTrainDeparture() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    
    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);
    
    String newLine = "Line2";
        
    String formattedTrainNumber = trainNumber;
    
    trainDepartureRegister.changeLineForTrainDeparture(formattedTrainNumber, newLine);

    TrainDeparture updatedTrainDeparture =
        trainDepartureRegister.findTrainDepartureByTrainNumber(formattedTrainNumber);
    System.err.println(updatedTrainDeparture);

    assertNotNull(updatedTrainDeparture);
    assertEquals(newLine, updatedTrainDeparture.getLine());

    assertTrue(trainDepartureRegister.doesTrainNumberExist(formattedTrainNumber));
  }

  @Test
  void testChangeLineForTrainDepartureWithNonExistingTrainNumber() {

    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);

    assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.changeLineForTrainDeparture("NonExistingTrain", "NewLine"));
  }


  @Test
    void testChangeDelayForTrainDeparture() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);

    int newDelay = 10;
    trainDepartureRegister.changeDelayForTrainDeparture(trainNumber, newDelay);

    assertEquals(newDelay, trainDepartureRegister
        .findTrainDepartureByTrainNumber(trainNumber).getDelay());
  }

  @Test
  void testChangeDelayForTrainDepartureWithNegativeNumber() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDepartureRegister.createNewTrainDeparture(
            departureTime, line, trainNumber, destination, delay, track);
  
      
    int newDelay = -10;
    assertThrows(IllegalArgumentException.class,
            () -> trainDepartureRegister.changeDelayForTrainDeparture(trainNumber, newDelay));
  }

  @Test
   void testChangeDelayForTrainDepartureWithNonExistingTrainNumber() {
    String nonExistingTrainNumber = "Train1";
    int newDelay = 10;

    assertThrows(IllegalArgumentException.class,
        () -> trainDepartureRegister.changeDelayForTrainDeparture(
          nonExistingTrainNumber, newDelay));
  }

  @Test
    void testFindTrainDepartureByTrainNumber() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;

    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);
    TrainDeparture foundDeparture = trainDepartureRegister
        .findTrainDepartureByTrainNumber(trainNumber);
    
    assertEquals(trainNumber, foundDeparture.getTrainNumber());
  }

  @Test
  void testFindTrainDepartureByTrainNumberWithNonExistingTrainNumber() {
    String nonExistingTrainNumber = "Train1";

    assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.findTrainDepartureByTrainNumber(nonExistingTrainNumber));
  }

  @Test
    void testDoesTrainNumberExist() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;

    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);

    assertTrue(trainDepartureRegister.doesTrainNumberExist(trainNumber));
  }

  @Test
    void testFindTrainDeparturesByDestination() {
    LocalTime departureTime = LocalTime.of(8, 0);
    String line = "Line1";
    String trainNumber = "Train1";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDepartureRegister.createNewTrainDeparture(
        departureTime, line, trainNumber, destination, delay, track);

    trainDepartureRegister.findTrainDeparturesByDestination(destination);

    assertTrue(trainDepartureRegister.doesDestinationExist(destination));
  }

  @Test
    void testFindTrainDeparturesByDestinationWithNonExistingDestination() {
    String nonExistingDestination = "Destination1";

    assertThrows(IllegalArgumentException.class,
          () -> trainDepartureRegister.findTrainDeparturesByDestination(nonExistingDestination));
  }

  @Test
    void testRemoveAllTrainsBeforeTime() {
    LocalTime departureTime1 = LocalTime.of(8, 0);
    LocalTime departureTime2 = LocalTime.of(9, 0);
    String line = "Line1";
    String trainNumber1 = "Train1";
    String trainNumber2 = "Train2";
    String destination = "Destination1";
    int delay = 0;
    int track = 1;
    trainDepartureRegister.createNewTrainDeparture(
        departureTime1, line, trainNumber1, destination, delay, track);
    trainDepartureRegister.createNewTrainDeparture(
        departureTime2, line, trainNumber2, destination, delay, track);

    LocalTime removeTime = LocalTime.of(8, 30);
    trainDepartureRegister.removeAllTrainsBeforeTime(removeTime);

    assertFalse(trainDepartureRegister.doesTrainDepartureExist(trainNumber1));
    assertTrue(trainDepartureRegister.doesTrainDepartureExist(trainNumber2));
  }
}