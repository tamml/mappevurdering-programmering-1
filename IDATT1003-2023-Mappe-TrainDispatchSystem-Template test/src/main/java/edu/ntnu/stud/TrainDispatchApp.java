package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * <h2>Class for TrainDeparture objects. </h2>
 * Contains a constructor to create TrainDeparture objects, and adds them to a list.
 * Contains main method for running the program.

 * @author Tam Minh Le
 * @version 1.0
 * @since 2023-11.12
 */
public class TrainDispatchApp {

  /**
   * <h2>Method for adding test data to the program. </h2>

   * @param trainDepartureRegister The TrainDepartureRegister object to add the test data to.

   */
  public static void testData(TrainDepartureRegister trainDepartureRegister) {

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "10:00"), "A", "123", "Oslo", 0, 1);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "10:15"), "B", "456", "Bergen", 0, 2);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "10:30"), "C", "789", "Trondheim", 20, 2);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "12:00"), "D", "101", "Stavanger", 0, 3);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "14:15"), "E", "102", "Kristiansand", 0, 4);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "08:50"), "F", "103", "Drammen", 40, 5);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse(
        "09:00"), "G", "104", "Bodø", 20, 5);

    trainDepartureRegister.createNewTrainDeparture(LocalTime.parse("15:00"),
        "H", "EF103", "Oslo Lufthavn", 0, 6);
  }

  /**
   * <h2>Main method for running the program. </h2>

   * @param args The arguments for the main method.
   */
  public static void main(String[] args) {
    TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();

    UserInterface userInterface = new UserInterface(trainDepartureRegister);

    userInterface.start();
  }

}