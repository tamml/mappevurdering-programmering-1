package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.HashMap;

/**
 * <h2>Class for TrainDepartureRegister objects.</h2>
 * Contains a constructor to create TrainDepartureRegister objects, and methods for manipulating the
 * TrainDeparture objects in the register.
 * The class stores TrainDeparture objects in a HashMap.
 * The class contains a method to remove all TrainDeparture objects with a updated departure time
 * before a given time.
 * <h3>The class contains the following methods: </h3>
 * <ul>
 * <li> printAllTrainDepartures(), createNewTrainDeparture(), </li>
 * <li> changeLineForTrainDeparture(),changeDelayForTrainDeparture(), </li>
 * <li> findTrainDepartureByTrainNumber(), doesTrainNumberExist(), </li>
 * <li> findTrainDeparturesByDestination(), doesDestinationExist(), </li>
 * <li> removeAllTrainsBeforeTime(). </li>
 * </ul>

 * @author Tam Minh Le
 * @version 1.0
 * @since 2023-11.12
 */
public class TrainDepartureRegister {
  private final HashMap<String, TrainDeparture> trainDepartures;

  public TrainDepartureRegister() {
    this.trainDepartures = new HashMap<>();
  }

  /**
   * <h2>Method for printing all TrainDeparture objects in the register.</h2>
   * The TrainDeparture objects are sorted by departure time.
   * The method prints the TrainDeparture objects to the console.
   */
  public void printAllTrainDepartures() {
    trainDepartures.values()
        .stream()
        .sorted(Comparator.comparing(TrainDeparture::getDepartureTime))
        .forEach(trainDeparture -> System.out.println(trainDeparture.toString()));
  }

  /**
   * <h2>Method for creating a new TrainDeparture object and adding it to the register.</h2>
   * The method takes the parameters and creates a new TrainDeparture object 
   * with the given parameters.

   * @param departureTime The departure time of the train.
   * @param line The line of the train.
   * @param trainNumber The train number of the train.
   * @param destination The destination of the train.
   * @param delay The delay of the train. 
   * @param track The track of the train.
   * @throws IllegalArgumentException If the train number already exists, 
   * @throws IllegalArgumentException If the train number contains spaces.
   * @throws IllegalArgumentException If the train number is empty.
   * @throws IllegalArgumentException If the line is empty.
   * @throws IllegalArgumentException If the track is not between 0 and 10.
   * @throws IllegalArgumentException If the departure time is null.
   */
  public void createNewTrainDeparture(LocalTime departureTime, String line, 
      String trainNumber, String destination, int delay, int track) {

    if (doesTrainNumberExist(trainNumber)) {
      throw new IllegalArgumentException("Train number already exists");
    } 
    if (trainNumber.contains(" ")) {
      throw new IllegalArgumentException("Train number cannot contain spaces");
    } 
    if (trainNumber.isEmpty()) {
      throw new IllegalArgumentException("Train number cannot be empty");
    } 
    if (line.isEmpty()) {
      throw new IllegalArgumentException("Line cannot be empty");
    } 
    if (track < 0 || track > 10) {
      throw new IllegalArgumentException("Track must be between 0 and 10");
    } 
    if (departureTime == null) {
      throw new IllegalArgumentException("Departure time cannot be null");
    } 
    TrainDeparture trainDeparture = 
          new TrainDeparture(departureTime, line, trainNumber, destination, delay, track, null);
    trainDepartures.put(trainNumber.toLowerCase(), trainDeparture);
  }


  /**
   * <h2>Method for checking if a train departure exists in the register.</h2>
   * The method takes the train number of the TrainDeparture object as a parameter.

   * @param trainNumber The train number of the TrainDeparture object.
   * @return True if the train departure exists in the register, false if it does not.
   */
  public boolean doesTrainDepartureExist(String trainNumber) {
    return trainDepartures.containsKey(trainNumber.toLowerCase());
  }

  /**
   * <h2>Method for changing the line of a TrainDeparture object. </h2>
   * The method takes the train number of the TrainDeparture object and the new line as parameters.
   * The method finds the TrainDeparture object with the given train number and changes the line
   * of the TrainDeparture object to the new line.

   * @param trainNumber The train number of the TrainDeparture object.
   * @param newLine The new line of the TrainDeparture object.
   * @throws IllegalArgumentException If the new line is empty.
   * @throws IllegalArgumentException If the train number is empty.
   * @throws IllegalArgumentException If the train number does not exist.
   */
  public void changeLineForTrainDeparture(String trainNumber, String newLine) {
    if (newLine.isEmpty()) {
      throw new IllegalArgumentException("New line cannot be empty");
    }
    if (trainNumber.isEmpty()) {
      throw new IllegalArgumentException("Train number cannot be empty");
    }
    if (!doesTrainNumberExist(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }

    trainDepartures.values()
            .stream()
            .filter(trainDeparture -> trainDeparture.getTrainNumber().equalsIgnoreCase(trainNumber))
            .forEach(trainDeparture -> trainDeparture.setLine(newLine));
  }


  /**
   * <h2>\Method for changing the delay of a TrainDeparture object. </h2>
   * The method takes the train number of the TrainDeparture object and the new delay as parameters.
   * The method finds the TrainDeparture object with the given train number and changes the delay
   * of the TrainDeparture object to the new delay.

   * @param trainNumber The train number of the TrainDeparture object.
   * @param newDelay The new delay of the TrainDeparture object.
   * @throws IllegalArgumentException If the train number is empty.
   * @throws IllegalArgumentException If the train number does not exist.
   */
  public void changeDelayForTrainDeparture(String trainNumber, int newDelay) {
    if (trainNumber.isEmpty()) {
      throw new IllegalArgumentException("Train number cannot be empty");
    }
    if (!doesTrainNumberExist(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }

    trainDepartures.values()
            .stream()
            .filter(trainDeparture -> trainDeparture.getTrainNumber().equalsIgnoreCase(trainNumber))
            .forEach(trainDeparture -> trainDeparture.setDelay(newDelay));
  }

  /**
   * <h2>Method for finding a TrainDeparture object by train number.</h2>
   * The method takes the train number of the TrainDeparture object as a parameter.
   * The method finds the TrainDeparture object with the given train number and returns it.

   * @param trainNumber The train number of the TrainDeparture object.
   * @return The TrainDeparture object with the given train number.
   * @throws IllegalArgumentException If the train number is empty.
   * @throws IllegalArgumentException If the train number does not exist.
   */
  public TrainDeparture findTrainDepartureByTrainNumber(String trainNumber) {
    if (trainNumber.isEmpty()) {
      throw new IllegalArgumentException("Train number cannot be empty");
    }
    if (!doesTrainNumberExist(trainNumber)) {
      throw new IllegalArgumentException("Train number does not exist");
    }
    
    return trainDepartures.get(trainNumber.toLowerCase());
  }

  /**
   * <h2>Method for checking if a train number exists in the register. </h2>
   * The method takes the train number as a parameter..

   * @param trainNumber The train number to check.
   * @return True if the train number exists in the register, false if it does not.
   */
  public boolean doesTrainNumberExist(String trainNumber) {
    boolean exist = trainDepartures.values()
            .stream()
            .anyMatch(trainDeparture -> trainDeparture.getTrainNumber().equals(trainNumber));
    
    return exist;
  }

  /**
   * <h2>Method for finding all TrainDeparture objects with a given destination. </h2>
   * The method takes the destination as a parameter.
   * The method finds all TrainDeparture objects with the given destination and prints them to the
   * console.

   * @param destination The destination of the TrainDeparture objects.
   * @throws IllegalArgumentException If the destination is empty.
   * @throws IllegalArgumentException If the destination does not exist.
   */
  public void findTrainDeparturesByDestination(String destination) {
    if (destination.isEmpty()) {
      throw new IllegalArgumentException("Destination cannot be empty");
    }
    if (!doesDestinationExist(destination)) {
      throw new IllegalArgumentException("Destination does not exist");
    }

    trainDepartures.values()
            .stream()
            .filter(trainDeparture -> trainDeparture.getDestination().equalsIgnoreCase(destination))
            .forEach(trainDeparture -> System.out.println(trainDeparture));
  }

  /**
   * <h2>Method for checking if a destination exists in the register. </h2>
   * The method takes the destination as a parameter.
   * The method checks if any TrainDeparture objects in the register have the given destination.

   * @param destination The destination to check.
   * @return True if the destination exists in the register, false if it does not.
   */
  public boolean doesDestinationExist(String destination) {
    return trainDepartures.values()
            .stream()
            .anyMatch(trainDeparture -> trainDeparture
            .getDestination().equalsIgnoreCase(destination));
  }

  /**
   * <h2>Method for removing a TrainDeparture object from the register.</h2>
   * The method takes the time as parameter.
   * The method removes all TrainDeparture objects with a updated departure time before the given
   * time.

   * @param time The departure time of the TrainDeparture object.
   */
  public void removeAllTrainsBeforeTime(LocalTime time) {
    trainDepartures.entrySet().removeIf(entry -> entry.getValue()
         .getUpdatedDepartureTime().isBefore(time));
  }

}