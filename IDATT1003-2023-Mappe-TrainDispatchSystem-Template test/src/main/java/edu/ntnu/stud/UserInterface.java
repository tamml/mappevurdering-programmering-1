package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.Scanner;

/**
 * <h2>Class for TrainDeparture objects. </h2>
 * Contains a menu for the user to interact with the program.
 * Contains methods for the user to interact with the program.
 * <h3> The menu contains the following methods: </h3>
 * 
 * <ul>
 * <li> showMenu </li>
 * <li> printAllTrainDepartures </li>
 * <li> createNewTrainDeparture </li>
 * <li> changeLineForTrainDeparture </li>
 * <li> changeDelayForTrainDeparture </li>
 * <li> findTrainDepartureByTrainNumber </li>
 * <li> findTrainDeparturesByDestination </li>
 * <li> changeSystemTime </li>
 * </ul>

 * @author Tam Minh Le
 * @version 1.0
 * @since 2023-11.12
 */
public class UserInterface {
  public Scanner in = new Scanner(System.in);
  private TrainDepartureRegister trainDepartureRegister = new TrainDepartureRegister();
  private LocalTime latestEnteredTime = LocalTime.parse("00:00");

  public UserInterface(TrainDepartureRegister trainDepartureRegister) {
    this.trainDepartureRegister = trainDepartureRegister;
  }

  private int showMenu() {
    int menuChoice = 0;
    System.out.println("1. Print all train departures");
    System.out.println("2. Create new train departure");
    System.out.println("3. Change line for a train departure");
    System.out.println("4. Change delay for a train departure");
    System.out.println("5. Find train departure by train number");
    System.out.println("6. Find train departure by destination");
    System.out.println("7. Change system time");
    System.out.println("0. Exit program");

    boolean validInput = false;

    while (!validInput) {
      try {
        if (in.hasNextLine()) {
          menuChoice = in.nextInt();
          in.nextLine();
          validInput = true;
        } else {
          System.err.println("Error: Invalid input. Please enter a number between 0 and 7");
          in.next(); 
        }
      } catch (Exception e) {
        System.err.println("Error: Invalid input. Please enter a valid integer.");
        in.next(); 
      }
    }
    return menuChoice;
  }

  /**
   * <h2>Starts the train dispatch application.</h2>
   * This method contains a while loop that runs until the user chooses to exit the program.
   * The user can choose between 7 different options, and the program will run the method
   * corresponding to the user's choice.
   * The user can also choose to exit the program.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void start() {
    TrainDispatchApp.testData(trainDepartureRegister);
    boolean running = true;
         
    while (running) {
      int menuChoice = showMenu();
      switch (menuChoice) {
        case 1:
          printAllTrainDepartures();
          break;
        case 2:
          createNewTrainDeparture();
          break;
        case 3:
          changeLineForTrainDeparture();
          break;
        case 4:
          changeDelayForTrainDeparture();
          break;
        case 5:
          findTrainDepartureByTrainNumber();
          break;
        case 6:
          findTrainDeparturesByDestination();
          break;
        case 7:
          changeSystemTime();
          break;
        case 0:
          System.out.println("Exiting program...");
          running = false;
          break;
        
        default: 
          System.out.println("Error: Invalid input. Please enter a number between 0 and 7");
          System.out.println("");
          break;
          
      }
    }
  }

  /**
   * <h2>Prints all of the train departures in the TrainDepartureRegister. </h2>
   * This method prints a banner before printing all of the train departures.
   */
  public void printAllTrainDepartures() {
    printTrainDepartureBanner();
    trainDepartureRegister.printAllTrainDepartures();
    System.out.println("");
  }

  /**
   * <h2>Creates a new train departure. </h2>
   * This method asks the user to enter the departure time, line, train number, destination, delay
   * and track for the new train departure.
   * The method then creates a new train departure with the given information.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void createNewTrainDeparture() {
    try {
      System.out.println("Please enter the departure time in the format HH:MM");
      LocalTime departureTime = LocalTime.parse(in.nextLine());

      System.out.println("Please enter the line");
      String line = in.nextLine();

      System.out.println("Please enter the train number");
      String trainNumber = in.nextLine();
     
      System.out.println("Please enter the destination");
      String destination = in.nextLine();

      System.out.println("Please enter the delay");
      int delay = Integer.parseInt(in.nextLine());

      System.out.println("Please enter the track");
      int track = Integer.parseInt(in.nextLine());

      trainDepartureRegister.createNewTrainDeparture(
          departureTime, line, trainNumber, destination, delay, track);
      System.out.println("Train departure created");

    } catch (Exception e) {
      userInputError(e);
    }
  }

  /**
   * <h2>Changes the line for a train departure. </h2>
   * This method asks the user to enter the train number for the train departure they want to
   * change the line for.
   * The method then asks the user to enter the new line for the train departure.
   * The method then changes the line for the train departure with the given train number.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void changeLineForTrainDeparture() {
    try {
      System.out.println("Please enter the train number");
      String trainNumber = in.nextLine();

      System.out.println("Please enter the new line");
      String newLine = in.nextLine();

      trainDepartureRegister.changeLineForTrainDeparture(trainNumber, newLine);
      System.out.println("Line changed successfully");

    } catch (Exception e) {
      userInputError(e);
    }
  }

  /**
   * <h2>Changes the delay for a train departure. </h2>
   * This method asks the user to enter the train number for the train departure they want to
   * change the delay for.
   * The method then asks the user to enter the new delay for the train departure.
   * The method then changes the delay for the train departure with the given train number.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void changeDelayForTrainDeparture() {
    try {
      System.out.println("Please enter the train number");
      String trainNumber = in.nextLine();

      System.out.println("Please enter the new delay (in minutes)");
      int newDelay = Integer.parseInt(in.nextLine());

      trainDepartureRegister.changeDelayForTrainDeparture(trainNumber, newDelay);
      System.out.println("Delay changed successfully");

    } catch (Exception e) {
      userInputError(e);
    }
  }

  /**
   * <h2>Finds a train departure by train number. </h2>
   * This method asks the user to enter the train number for the train departure they want to find.
   * The method then finds the train departure with the given train number and prints it.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void findTrainDepartureByTrainNumber() {
    try {
      System.out.println("Please enter the train number");
      String trainNumber = in.nextLine();

      if (trainDepartureRegister.doesTrainNumberExist(trainNumber)) {
        printTrainDepartureBanner();

        TrainDeparture trainDeparture = 
            trainDepartureRegister.findTrainDepartureByTrainNumber(trainNumber);
        if (trainDeparture != null) {
          System.out.println(trainDeparture);
        }
        System.out.println("");
      } else {
        System.err.println("Error: Train number does not exist.");
        System.out.println("");
      }
    } catch (Exception e) {
      userInputError(e);
    }
  }

  /**
   * <h2>Finds all train departures by destination. </h2>
   * This method asks the user to enter the destination for the train departures they want to find.
   * The method then finds all train departures with the given destination and prints them.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void findTrainDeparturesByDestination() {
    try {
      System.out.println("Please enter the destination");
      String destination = in.nextLine();

      if (trainDepartureRegister.doesDestinationExist(destination)) {
        printTrainDepartureBanner();
        trainDepartureRegister.findTrainDeparturesByDestination(destination);
        System.out.println("");
      } else {
        System.err.println("Error: Destination does not exist.");
        System.out.println("");
      }
    } catch (Exception e) {
      userInputError(e);
    }
  }

  /**
   * <h2>Changes the system time. </h2>
   * This method asks the user to enter the new time for the system.
   * The method then changes the system time to the given time.
   * If the user enters an invalid input, the program will print an error message and ask the user
   * to enter a valid input.
   */
  public void changeSystemTime() {
    try {
      System.out.println("Please enter the new time in the format HH:MM");
      LocalTime newTime = LocalTime.parse(in.nextLine());

      if (newTime.isAfter(latestEnteredTime)) {
        trainDepartureRegister.removeAllTrainsBeforeTime(newTime);
        System.out.println("Time successfully changed to " + newTime);
        latestEnteredTime = newTime;
      } else {
        System.err.println(
            "Error: New departure time should be later than the latest entered time.");
      }
    } catch (Exception e) {
      System.err.println("Error: Invalid input. Please check your input format for the time.");
    }
    System.out.println("");
  }

  /**
   * <h2>Prints an error message if the user enters an invalid input.</h2>

   * @param e The exception that was thrown.
   */
  private void userInputError(Exception e) {
    System.err.println(
        "Error: Invalid input. Please check your input and make sure it is in the correct format.");
    System.out.println("");
  }

  /**
   * <h2>Prints a banner for the train departures.</h2>
   */
  private void printTrainDepartureBanner() {
    System.out.println(
        "---------------------Printing out all of the train departures-----------------------");
    System.out.printf("%-13s%-10s%-10s%-20s%-10s%-10s%-10s%n",
            "Departure", "Line", "Train", "Destination", "Delay", "Track", "Updated Departure");
  }
}