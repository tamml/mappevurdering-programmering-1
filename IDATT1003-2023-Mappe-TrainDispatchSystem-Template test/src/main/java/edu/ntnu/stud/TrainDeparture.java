package edu.ntnu.stud;

import java.time.LocalTime;

/**
 * <h2>Class for TrainDeparture objects.</h2>
 * Contains a constructor to create TrainDeparture objects, and getters and setters for the fields.

 * @author Tam Minh Le
 * @version 1.0
 * @since 2023-11.12
 */
public class TrainDeparture {
  private LocalTime departureTime;
  private String line;
  private final String trainNumber;
  private final String destination;
  private int delay;
  private final int track;

  /**
   * <h2>Constructor for TrainDeparture objects.</h2>
   * Creates a TrainDeparture object with the given parameters.

   * @param departureTime The departure time of the train.
   * @param line The line of the train. 
   * @param trainNumber The train number of the train.
   * @param destination The destination of the train.
   * @param delay The delay of the train.
   * @param track The track of the train.
   * @param updatedDepartureTime The updated departure time of the train.
   */
  public TrainDeparture(LocalTime departureTime, 
      String line, 
      String trainNumber, 
      String destination, 
      int delay, 
      int track, 
      LocalTime updatedDepartureTime) {  
    this.departureTime = departureTime;
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.delay = delay;
    this.track = track;
  }

  /**
   * <h2>Method for getting the departure time of a TrainDeparture object.</h2>

   * @return The departure time of the TrainDeparture object.
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }  

  /**
   * <h2>Method for getting the line of a TrainDeparture object.</h2>

   * @return The line of the TrainDeparture object.
   */
  public String getLine() {
    return line;
  } 

  /**
 *<h2> Method for setting the line of a TrainDeparture object.</h2>

 * @return The new line of the TrainDeparture object.
 */
  public String setLine(String newLine) {
    return this.line = newLine;
  } 

  /**
   * <h2>Method for getting the train number of a TrainDeparture object.</h2>

   * @return The train number of the TrainDeparture object.
   */
  public String getTrainNumber() {
    return trainNumber;
  } 

  /**
   * <h2>Method for getting the destination of a TrainDeparture object.</h2>

   * @return The destination of the TrainDeparture object.
   */
  public String getDestination() {
    return destination;
  } 

  /**
   * <h2>Method for getting the track of a TrainDeparture object.</h2>

   * @return The track of the TrainDeparture object.
   */
  public int getTrack() {
    return track;
  }
  
  /**
   * <h2>Method for getting the delay of a TrainDeparture object.</h2>

   * @return The delay of the TrainDeparture object.
   */
  public int getDelay() {
    return delay;
  } 

  /**
   * <h2>Method for setting the delay of a TrainDeparture object.</h2>
   * Throws an exception if the delay is negative.

   * @return The new delay of the TrainDeparture object.
   * @throws IllegalArgumentException If the delay is negative.
   */
  public int setDelay(int delay) {
    if (delay < 0) {
      throw new IllegalArgumentException();
    }
    return this.delay = delay;
  } 

  /**
   * <h2>Method for setting the departure time of a TrainDeparture object.</h2>

   * @param departureTime The new departure time of the TrainDeparture object.
   */
  public void setDepartureTime(LocalTime departureTime) {
    this.departureTime = departureTime;
  } 

  /**
   * <h2>Method for calculating updated departure time. </h2>
   * Adds the delay to the departure time.

   * @return The updated departure time.
   */
  public LocalTime getUpdatedDepartureTime() {
    return departureTime.plusMinutes(delay);
  }

  /**
   * <h2>Method for printing TrainDeparture objects. </h2>
   * If the delay is 0, the delay and updated departure time is not printed.
   */
  @Override
  public String toString() {
    if (delay == 0) {
      return String.format("%-13s%-10s%-10s%-20s%-10s%-10s%-10s", 
      departureTime, line, trainNumber, destination, "", track, "");

    } else {
      return String.format("%-13s%-10s%-10s%-20s%-10s%-10s%-10s",
      departureTime, line, trainNumber, destination, delay, track, getUpdatedDepartureTime());
    }
  }

}
    

